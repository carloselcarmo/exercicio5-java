package br.com.Itau;

import java.util.Map;

public class Bingo
{
    CacaNiquel cacaNiquel;

    public Bingo()
    {
        this.cacaNiquel = new CacaNiquel(3);
    }

    public void exibirOpcoesCacaNiquel()
    {
        Integer opcao;
        do
        {
            Map<String, Integer> opcoesJogo = IO.solicitarOpcaoJogoCacaNivel();
            opcao = opcoesJogo.get("opcao");

            if(opcao == 1)
                this.cacaNiquel.jogar();

        } while(opcao != 2);
    }
}

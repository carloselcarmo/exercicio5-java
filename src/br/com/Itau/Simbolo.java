package br.com.Itau;

public enum Simbolo
{
    Banana(10),
    Framboesa(50),
    Moeda(100),
    Sete(300);

    private int valor;

    public int getValor()
    {
        return valor;
    }

    Simbolo(int valor)
    {
        this.valor = valor;
    }
}

package br.com.Itau;

import java.util.Random;

public class Slot
{
    public Simbolo sortear()
    {
        Random random = new Random();

        Integer numeroSorteado = random.nextInt(Simbolo.values().length);

        return Simbolo.values()[numeroSorteado];
    }
}

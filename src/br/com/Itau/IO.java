package br.com.Itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO
{
    public static void imprimir(Simbolo simbolo)
    {
        System.out.print(simbolo + "(" + simbolo.getValor() + ") ");
    }

    public static void exibirPontuacao(Integer pontuacao)
    {
        System.out.println("Pontuação: " + pontuacao);
    }

    public static Map<String, Integer> solicitarOpcaoJogoCacaNivel()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Caça-Níquel -> ");
        System.out.printf("O que você deseja fazer? ");
        System.out.printf("(1) Jogar");
        System.out.printf("(2) Sair");
        Integer opcao = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("opcao", opcao);

        return dados;
    }
}

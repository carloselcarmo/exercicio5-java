package br.com.Itau;

import java.util.ArrayList;
import java.util.List;

public class CacaNiquel
{
    private List<Slot> slots;

    public CacaNiquel(Integer quantidadeSlots)
    {
        slots = new ArrayList<>();

        for(int i = 0 ; i < quantidadeSlots; i++)
        {
            slots.add(new Slot());
        }
    }

    public void jogar()
    {
        List<Simbolo> simbolosSorteados = new ArrayList<>();

        for(Slot slot : slots)
        {
            Simbolo simboloSorteado = slot.sortear();
            simbolosSorteados.add(simboloSorteado);
            IO.imprimir(simboloSorteado);
        }

        exibirPontuacao(simbolosSorteados);
    }

    private void exibirPontuacao(List<Simbolo> simbolos)
    {
        Integer pontuacao = 0;
        Simbolo simboloAnterior = null;
        Integer quantidadeSimbolosIguais = 0;

        for (Simbolo simbolo: simbolos)
        {
            //Soma o valor de cada símbolo
            pontuacao += simbolo.getValor();

            //Só soma mais um na quantidade de símbolos iguais se for igual ao anterior
            if(simboloAnterior == null || simbolo == simboloAnterior)
            {
                quantidadeSimbolosIguais++;
                simboloAnterior = simbolo;
            }
        }

        //Caso a quantidade de símbolos iguais seja igual a quantidade de símbolos sorteados multiplica a pontuação por 100
        if(simbolos.size() == quantidadeSimbolosIguais)
        {
            pontuacao = pontuacao * 100;
        }

        IO.exibirPontuacao(pontuacao);
    }
}
